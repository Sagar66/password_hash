#!/bin/bash
##################
#% This script encrypts clean passwords from file to encrypted one
##################
# BEGAIN
INPUT_FILE="clean_pass.txt"
OUTPUT_FILE="pass_encrypted.text"
if [ -f "$INPUT_FILE" ]; then
	printf -- '\033[1;95m Processing...\033[0m\n'
else
	printf -- '\033[33mPlease create $INPUT_FILE in root directory and insert eatch clean passwords in new lines without whitespaces.\033[0m\n'
fi
if [ -f "${INPUT_FILE}" ]; then
	rm ./"${OUTPUT_FILE}"
fi
while IFS= read -r line
do
	#Clean White Spaces
	clean=$(echo -e "${line}" | tr -d '[:space:]')
	#Don't remove white spaces
	#clean="${line}"
	echo -e "\e[1;92mEncrypting : $clean\c"
	echo -e "\"$clean\c" >> ${OUTPUT_FILE}
	echo -e "\",\"\c" >> ${OUTPUT_FILE}
	htpasswd -bnBC 10 "" "${clean}" | tr -d ':\n' | sed 's/$2y/$2a/' >> ${OUTPUT_FILE}
	echo -e \" >> ${OUTPUT_FILE}
	echo -e "\t\t\t\t\t\t$(printf '%20s\n' | tr ' ' -)>Done"
done < ${INPUT_FILE}
printf -- '\n';	
printf -- 'Done\033\n';
