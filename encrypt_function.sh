#!/bin/bash
##################
#% This script encrypts clean passwords from file to encrypted one
##################
# BEGAIN
#including init.d function
#. /etc/init.d/functions

INPUT_FILE="clean_pass.txt"
OUTPUT_FILE="pass_encrypted.text"
if [ -f "$INPUT_FILE" ]; then
	echo Processing...
else
	echo "Please create $INPUT_FILE in root directory and insert eatch clean passwords in new lines without whitespaces."
fi
if [ -f "${INPUT_FILE}" ]; then
	rm ./"${OUTPUT_FILE}"
fi
while IFS= read -r line
do
	#Clean White Spaces
	clean=$(echo -e "${line}" | tr -d '[:space:]')
	#Don't remove white spaces
	#clean="${line}"
	echo -e "Encrypting : $clean\c"
	echo -e "\"$clean\c" >> ${OUTPUT_FILE}
	echo -e "\",\"\c" >> ${OUTPUT_FILE}
	htpasswd -bnBC 10 "" "${clean}" | tr -d ':\n' | sed 's/$2y/$2a/' >> ${OUTPUT_FILE}
	echo -e \" >> ${OUTPUT_FILE}
	echo -e "\t\t\t\t$(printf '%20s\n' | tr ' ' -)>Done"
done < ${INPUT_FILE}
	
echo completed
